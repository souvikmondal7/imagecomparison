
package com.catalina.imagecomparison.listener;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.catalina.imagecomparison.entities.ImageData;
import com.catalina.imagecomparison.entities.TargetProcessDetail;
import com.catalina.imagecomparison.resources.ImageCompResource;

@Service
public class KafkaListener {
	private static Logger logger = LoggerFactory.getLogger(KafkaListener.class);

	@Value("${kafka.topic}")
	public static final String kafkaTopic = "imageComp";

	@Value("${targetprocess.topic}")
	private String targetProcessTopic;

		
//	@Value("${file.location.to.save.ts.script}")
//	String fileLocationToSaveTsScript;
//
//	@Value("${linux.path.for.ts.script}")
//	String linuxPathForTsScript;
	
	
	@Autowired
	KafkaTemplate<String, TargetProcessDetail> kafkaTemplate;

	@org.springframework.kafka.annotation.KafkaListener(topics = kafkaTopic, containerFactory = "kafkaListenerContainerFactory")
	public void listener(ImageData data) throws InterruptedException {
		try {
			System.out.println(data);
			logger.info("data received from imageComp: "+data.toString());
			ImageData result = ImageCompResource.traverseJson(data);
			result.getTargetProcessDetail().setStatus(result.getStatus()==true?"Passed":"OnHold");
			logger.info("status set");
			System.out.println(result);
			logger.info("result is "+result.toString());
			if(result.getStatus()==true) {
			result.getTargetProcessDetail().setComment("Image comparison Passed and result is: " +result.toString());
							logger.info("comment set");
			}else {
				result.getTargetProcessDetail().setComment("Image comparison Failed and result is: " +result.toString());
				logger.info("comment set");
			}
			logger.info("sending data to targetprocess microservice:"+result.getTargetProcessDetail());
			kafkaTemplate.send(targetProcessTopic,result.getTargetProcessDetail());
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
