package com.catalina.imagecomparison.config;

import org.opencv.core.Core;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenCVConfiguration {

    static {
        System.out.println("xxxxxxxxxxxxxxxxxx Loading Lib");
        //System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        org.apache.tomcat.jni.Library.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }
}
