package com.catalina.imagecomparison.config;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;

import com.catalina.imagecomparison.entities.TargetProcessDetail;

@Configuration
public class KafkaProducerConfig {

	@Value("${kafka.broker.server}")
	String address;

	
	@Bean
	public ProducerFactory kafkaProducerFactory() {
		Map<String, Object> config = new HashMap<>();
//		config.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "10.10.108.58:9092");
		config.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, address);
		config.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		config.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
		return new DefaultKafkaProducerFactory<>(config,new StringSerializer(), new JsonSerializer<TargetProcessDetail>());
	}
	
	@Bean
	public KafkaTemplate<String,TargetProcessDetail> kafkaTemplate() {
		return new KafkaTemplate<String,TargetProcessDetail>(kafkaProducerFactory());
	}
}
