package com.catalina.imagecomparison.services;

public interface Cropper {
	public Image cropImage(Image image, int x, int y, int width, int height);
}
