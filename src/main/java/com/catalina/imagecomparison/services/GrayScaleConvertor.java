package com.catalina.imagecomparison.services;

public interface GrayScaleConvertor {

	public Image convert(Image image);
}
