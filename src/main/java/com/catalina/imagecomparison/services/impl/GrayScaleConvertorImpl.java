package com.catalina.imagecomparison.services.impl;

import com.catalina.imagecomparison.services.GrayScaleConvertor;
import com.catalina.imagecomparison.services.Image;

import java.awt.*;
import java.awt.image.BufferedImage;

public class GrayScaleConvertorImpl implements GrayScaleConvertor {
	private int width;
	private int height;

	@Override
	public com.catalina.imagecomparison.services.Image convert(Image image) {
		// TODO Auto-generated method stub
		BufferedImage img = image.getImage();
		width = img.getWidth();
		height = img.getHeight();

		for (int i = 0; i < height; i++) {

			for (int j = 0; j < width; j++) {

				Color c = new Color(img.getRGB(j, i));
				int red = (int) (c.getRed() * 0.299);
				int green = (int) (c.getGreen() * 0.587);
				int blue = (int) (c.getBlue() * 0.114);
				Color newColor = new Color(red + green + blue,

						red + green + blue, red + green + blue);

				img.setRGB(j, i, newColor.getRGB());
			}
		}
		image.setImage(img);
		return image;
	}

}
