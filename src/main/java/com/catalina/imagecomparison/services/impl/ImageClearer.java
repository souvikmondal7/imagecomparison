package com.catalina.imagecomparison.services.impl;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.Point;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import com.catalina.imagecomparison.services.Image;

public class ImageClearer {

	
	public static Image temp(Image img) throws IOException {

		BufferedImage bufferedImage = img.getImage();

		Mat src = new Mat(bufferedImage.getHeight(), bufferedImage.getWidth(), CvType.CV_8UC3);
		byte[] data = ((DataBufferByte) bufferedImage.getRaster().getDataBuffer()).getData();
		src.put(0, 0, data);

		Mat dst = new Mat();
		Imgproc.cvtColor(src, dst, Imgproc.COLOR_BGR2GRAY);

		Mat small = new Mat();
		Imgproc.pyrDown(dst, small);
		final Size kernelSize = new Size(3, 3);
		final Point anchor = new Point(-1, -1);
		final int iterations = 3;

		Mat kernel = Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, kernelSize);

		Imgproc.GaussianBlur(small, small, new Size(3, 3), 0);
		Imgproc.erode(small, small, kernel, anchor, iterations);
		Imgproc.dilate(small, small, kernel);

		Imgproc.adaptiveThreshold(small, small, 255, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C, Imgproc.THRESH_BINARY, 45, 2);

		Mat kernel2 = Imgproc.getGaussianKernel(6, 2);

		Imgproc.dilate(small, small, kernel2);
		Imgproc.erode(small, small, kernel);
		MatOfByte mob = new MatOfByte();
		// .jpg is not working
		// Imgcodecs.imencode(".jpg", dst, mob);
		Imgcodecs.imencode(".bmp", small, mob);
		byte ba[] = mob.toArray();

		BufferedImage bi = ImageIO.read(new ByteArrayInputStream(ba));

		img.setImage(bi);
		return img;
	}
}
