package com.catalina.imagecomparison.services.impl;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.Scalar;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import com.catalina.imagecomparison.services.Image;

public class Invert {

	public static Image inVert(Image img) throws IOException {
		// System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		BufferedImage bufferedImage = img.getImage();

		Mat src = new Mat(bufferedImage.getHeight(), bufferedImage.getWidth(), CvType.CV_8UC3);
		byte[] data = ((DataBufferByte) bufferedImage.getRaster().getDataBuffer()).getData();
		src.put(0, 0, data);

		Mat dst = new Mat();
		Imgproc.cvtColor(src, dst, Imgproc.COLOR_BGR2GRAY);
//		Imgproc.adaptiveThreshold(src, src, 255, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C, Imgproc.THRESH_BINARY, 7, 1);
		Core.bitwise_not(src, dst);
		Core.multiply(dst, new Scalar(255), dst);

		MatOfByte mob = new MatOfByte();
		// .jpg is not working
		// Imgcodecs.imencode(".jpg", dst, mob);
		Imgcodecs.imencode(".bmp", dst, mob);
		byte ba[] = mob.toArray();

		BufferedImage bi = ImageIO.read(new ByteArrayInputStream(ba));

		img.setImage(bi);
		return img;

	}
}
