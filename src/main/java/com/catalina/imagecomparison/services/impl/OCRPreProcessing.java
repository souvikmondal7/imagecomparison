package com.catalina.imagecomparison.services.impl;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import com.catalina.imagecomparison.services.Image;

public class OCRPreProcessing {

	public static int count = 0;

	public static Image preProcessing(Image img, double length, double breadth) throws IOException {
		// TODO Auto-generated method stub
		img.writeImage("testing", "png", "./report");
		img.covertToFresh();
		BufferedImage bufferedImage = img.getImage();
		Mat src = new Mat(bufferedImage.getHeight(), bufferedImage.getWidth(), CvType.CV_8UC3);
		byte[] data = ((DataBufferByte) bufferedImage.getRaster().getDataBuffer()).getData();
		src.put(0, 0, data);
		writeImage(src, "1.before");
		Mat dst = new Mat();
		int dilation_size = 1;
		Mat element = Imgproc.getStructuringElement(Imgproc.MORPH_RECT,
				new Size(2 * dilation_size + 1, 2 * dilation_size + 1));

		Imgproc.resize(src, src, new Size(length, breadth), 1.7, 1.7, Imgproc.INTER_CUBIC);

//		writeImage(src, "1.resize_");
		Imgproc.cvtColor(src, dst, Imgproc.COLOR_BGR2GRAY);
//		writeImage(dst, "2.grayScale_");
//		Imgproc.adaptiveThreshold(src, src, 255, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C, Imgproc.THRESH_BINARY, 7, 1);
		Imgproc.dilate(dst, dst, element);
//		writeImage(dst, "3.dialated_");
		Imgproc.erode(dst, dst, element);
//		writeImage(dst, "4.erossion_");
		Imgproc.GaussianBlur(dst, dst, new Size(5, 5), 0);
//		writeImage(dst, "5.gaussianFilter_");
		Imgproc.threshold(dst, dst, 0, 255, Imgproc.THRESH_BINARY + Imgproc.THRESH_OTSU);
//		writeImage(dst, "6.threshold_");
		// Imgproc.GaussianBlur(dst, dst, new Size(5, 5), 0);

		MatOfByte mob = new MatOfByte();
		// .jpg is not working
		// Imgcodecs.imencode(".jpg", dst, mob);
		Imgcodecs.imencode(".bmp", dst, mob);
		byte ba[] = mob.toArray();

		BufferedImage bi = ImageIO.read(new ByteArrayInputStream(ba));

		img.setImage(bi);
//		count++;
		return img;
	}

	private static void writeImage(Mat dst, String name) throws IOException {
		MatOfByte mob = new MatOfByte();
		// .jpg is not working
		// Imgcodecs.imencode(".jpg", dst, mob);
		Imgcodecs.imencode(".bmp", dst, mob);
		byte ba[] = mob.toArray();

		BufferedImage bi = ImageIO.read(new ByteArrayInputStream(ba));
		Image img = new Image();
		img.setImage(bi);
		img.writeImage(name + count, "png", "./report");

	}

}
