package com.catalina.imagecomparison.services.impl;

import com.catalina.imagecomparison.services.Cropper;
import com.catalina.imagecomparison.services.Image;

public class ImageCropperImpl implements Cropper {
	@Override
	public Image cropImage(Image image, int x, int y, int width, int height) {
		Image img = new Image();
		img.setImage(image.getImage().getSubimage(x, y, width, height));
		return img;
	}
}
