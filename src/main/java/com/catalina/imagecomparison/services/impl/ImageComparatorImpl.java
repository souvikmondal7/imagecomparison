package com.catalina.imagecomparison.services.impl;

import com.catalina.imagecomparison.entities.*;
import com.catalina.imagecomparison.services.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

public class ImageComparatorImpl extends ImageCompartor {

	private Logger logger = LoggerFactory.getLogger(ImageComparatorImpl.class);

	@Override
	public ImageData compareImageFromJson(Image img, Image img2, ImageData data) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		Factory factory = new Factory();
		boolean result = true;

		// TODO: Add teh log with teh side by side image of before and after

		logger.info("Converting image to gray scale");
		// convert to gray scale

		img.writeImage("BeforeGrayScale" + imageNO, "jpg", "./report/img");
		Image temp = new Image();
		temp.readImage("BeforeGrayScale" + imageNO, "jpg", "./report/img");

		GrayScaleConvertor gsc = factory.getGrayScaleConveterInstance("normal");
		// img = gsc.convert(img);
		// img2 = gsc.convert(img2);
		logSideBySideImage(img, temp, "GrayScale");

		// TODO add the information read from the actual barcode
		logger.info("Reading barCode");
		BarCode barCode = data.getBarCode();
		// compare bar code values
		if (barCode == null)
			logger.info("BarCode not found in json");
		else if (!comapreBarCodeValuesWithJson(img, barCode)) {
			logger.info("Bar code values are not matched");
			result &= false;
		}
		if (barCode != null) {
			barCode.setStatus(result);
			data.setBarCode(barCode);
		}

		// TODO add the information read from the actual QR Code
		logger.info("Reading qrcode image");
		QRCode qrCode = data.getQrCode();
		// compare bar code values
		if (qrCode == null)
			logger.info("QrCode not found in json");
		if (!comapreQRCodeValuesWithJson(img, qrCode)) {
			logger.info("qrCode is not matched");
			result &= false;
		}
		if (qrCode != null) {
			qrCode.setStatus(result);
			data.setQrCode(qrCode);
		}
		// TODO: Add the log with the side by side image of before and after
		temp = img;
		logger.info("Rotating a image");
		// Getting orientation of image
		String direction = data.getImageOrientation();
		int rotate = 0;
		switch (direction.toLowerCase()) {
		case "left":
			rotate++;
		case "up":
			rotate++;
		case "right":
			rotate++;
			break;
		}

		img.writeImage("BeforeRotateImage" + imageNO, "jpg", "./report/img");
		temp = new Image();
		temp.readImage("BeforeRotateImage" + imageNO, "jpg", "./report/img");

		Rotater rotater = factory.getRotatorInstance("normal");
		// rotate image as per requirement
		for (int i = 0; i < rotate; i++) {
			try {
				img = rotater.rotate(img);
				img2 = rotater.rotate(img2);
			} catch (IOException e) {
				System.out.println(e);
			}
		}

		logSideBySideImage(img, temp, "RotateImage");

		logger.info("checking for the  images");

		// TODO: Add the log with the side by side image of before and after
		// Also can we mark the co-ordinates of teh image being checked in the
		// log. This
		// would be nice to have but we can work on this later
		List<StaticImage> element = data.getStaticImageSections();
		img.writeImage("temp1", "png", "./");
		if (element == null || element.size() == 0)
			logger.info("Dont have  image section");
		else {
			result &= Image(element, img, img2);
			if (result)
				logger.info(" images are matched");
			else
				logger.info(" images are not matched");
		}

		// TODO: Add the log with the side by side image of before and after
		// get horizontalMessages elements
		logger.info("Checking the horizontal messages");
		List<Message> horizontalMessages = data.getHorizontalMessages();
		if (horizontalMessages == null || horizontalMessages.size() == 0)
			logger.info("Not elment found");
		else if (horizontalMessages != null && horizontalMessages.size() > 0) {
			List<Message> resultList = checkMessages(img2, horizontalMessages);
			for (Message message : resultList)
				result &= message.getStatus();
			if (!result) {
				logger.info("Horizontal messages are not matched");
				data.setHorizontalMessages(resultList);
				boolean tempResult = false;
				for (Message message : resultList) {
					if (message.getStatus())
						continue;
					int count = 0;
					if (message.getAlternateMessages() != null && message.getAlternateMessages().size() > 0) {
						for (String alternateMessage : message.getAlternateMessages()) {
							System.out.println(alternateMessage + " " + alternateMessage
									.compareTo(message.getExtractedMessage().replace("\\n", "").trim()));
							if (alternateMessage.contains(message.getExtractedMessage().replace("\\n", "").trim())) {
								count++;
								message.setStatus(true);
							}
						}
						System.out.println("Count " + count + " size " + message.getAlternateMessages().size());
						if (count == message.getAlternateMessages().size())
							tempResult = true;
					} else {
						tempResult = result;
					}
				}
				boolean updatingResult = true;
				for (Message message : resultList) {
					updatingResult = updatingResult && message.getStatus();
				}
				System.out.println("Final Result" + tempResult);
				result = tempResult;
				data.setStatus(updatingResult);
			} else
				data.setStatus(result);
		}
		img.writeImage("temp2", "png", "./");

		// rotate image
		// TODO: Add the log with the side by side image of before and after
		// get vertical elements
		int vertRotate = 1;
		if (data.getVerticalRotate() != null)
			vertRotate = data.getVerticalRotate();
		for (int i = 0; i < vertRotate; i++) {
			try {
				img = rotater.rotate(img);
				img2 = rotater.rotate(img2);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		img.writeImage("temp3", "png", "./");
		logger.info("Checking the vertical messages");
		List<Message> vertical = data.getVerticalMessages();
		if (vertical == null || vertical.size() == 0)
			logger.info("Vertical messages are not presesnt");
		if (vertical != null && vertical.size() > 0) {
			List<Message> resultList = checkMessages(img2, vertical);
			for (Message message : resultList)
				result &= message.getStatus();
			if (!result) {
				logger.info("Horizontal messages are not matched");
				data.setVerticalMessages(resultList);
				boolean tempResult = false;
				for (Message message : resultList) {
					if (message.getStatus())
						continue;
					int count = 0;
					if (message.getAlternateMessages() != null && message.getAlternateMessages().size() > 0) {
						for (String alternateMessage : message.getAlternateMessages()) {
							System.out.println(alternateMessage + " " + alternateMessage
									.compareTo(message.getExtractedMessage().replace("\\n", "").trim()));
							if (alternateMessage.contains(message.getExtractedMessage().replace("\\n", "").trim())) {
								count++;
								message.setStatus(true);
							}
						}
						System.out.println("Count " + count + " size " + message.getAlternateMessages().size());
						if (count == message.getAlternateMessages().size())
							tempResult = true;
					} else {
						tempResult = result;
					}
				}
				boolean updatingResult = true;
				for (Message message : resultList) {
					updatingResult = updatingResult && message.getStatus();
				}
				System.out.println("Final Result" + tempResult);
				result = tempResult;
				data.setStatus(updatingResult);
			} else
				data.setStatus(result);
		}
		data.setStatus(result);
		return data;
	}

}
