package com.catalina.imagecomparison.services.impl;

import com.catalina.imagecomparison.services.Image;
import com.catalina.imagecomparison.services.Rotater;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.imgcodecs.Imgcodecs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.ByteArrayInputStream;
import java.io.IOException;

public class RotaterImpl implements Rotater {
	private Logger logger = LoggerFactory.getLogger(RotaterImpl.class);
	//static{ System.loadLibrary(Core.NATIVE_LIBRARY_NAME); }
/*	public static void main(String[] args) {
		try {
			System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
			Mat src = Imgcodecs
					.imread("D:/Workspace/Beta/test-output/Image/grayscale/Gm2.png");

			Mat dst = new Mat();
			Core.rotate(src, dst, Core.ROTATE_90_COUNTERCLOCKWISE);

			Imgcodecs.imwrite(
					"D:/Workspace/Beta/test-output/Image/rotated/Rm2.png", dst);

		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}
	}
*/
	@Override
	public Image rotate(Image img) throws IOException {
		//System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		logger.info("received image: "+img);
		BufferedImage bufferedImage = img.getImage();
		logger.info("image is: "+bufferedImage+" with height: "+bufferedImage.getHeight()+" with width: "+bufferedImage.getWidth());
		Mat src = new Mat(bufferedImage.getHeight(), bufferedImage.getWidth(),
				CvType.CV_8UC3);
		byte[] data = ((DataBufferByte) bufferedImage.getRaster()
				.getDataBuffer()).getData();
		src.put(0, 0, data);

		Mat dst = new Mat();
		Core.rotate(src, dst, Core.ROTATE_90_COUNTERCLOCKWISE);

		MatOfByte mob = new MatOfByte();
		// .jpg is not working 
		//Imgcodecs.imencode(".jpg", dst, mob);
		Imgcodecs.imencode(".bmp", dst, mob);
		byte ba[] = mob.toArray();

		BufferedImage bi = ImageIO.read(new ByteArrayInputStream(ba));

		img.setImage(bi);
		return img;
	}
}
