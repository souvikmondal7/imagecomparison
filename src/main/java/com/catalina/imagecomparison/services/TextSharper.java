package com.catalina.imagecomparison.services;

public interface TextSharper {

	public Image sharpeText(Image img);
}
