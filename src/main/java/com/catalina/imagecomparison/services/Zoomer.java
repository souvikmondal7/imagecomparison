package com.catalina.imagecomparison.services;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;

public class Zoomer {

	public static Image zoomImage(Image image, float zoomLevel) {
		int newImageWidth = (int) (image.getImage().getWidth() * zoomLevel);
		int newImageHeight = (int) (image.getImage().getHeight() * zoomLevel);
		BufferedImage resizedImage = new BufferedImage(newImageWidth, newImageHeight, java.awt.Image.SCALE_DEFAULT);
		Graphics2D g = resizedImage.createGraphics();
		g.drawImage(image.getImage(), 0, 0, newImageWidth, newImageHeight, null);
		g.dispose();
		Kernel kernel = new Kernel(3, 3, new float[] { -1, -1, -1, -1, 9, -1, -1, -1, -1 });
		BufferedImageOp op = new ConvolveOp(kernel);
		resizedImage = op.filter(resizedImage, null);
		image.setImage(resizedImage);
		return image;
	}
}
