package com.catalina.imagecomparison.services;

public interface CompareImage {
	public double compareImage(Image img, Image img2);
}
