package com.catalina.imagecomparison.services;

import java.io.IOException;

public interface Rotater {
	public Image rotate(Image img) throws IOException;
}
