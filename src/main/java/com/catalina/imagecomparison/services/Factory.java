package com.catalina.imagecomparison.services;

import com.catalina.imagecomparison.services.impl.GrayScaleConvertorImpl;
import com.catalina.imagecomparison.services.impl.ImageCropperImpl;
import com.catalina.imagecomparison.services.impl.RotaterImpl;

public class Factory {

	public Cropper getCropperInstance(String text) {
		if (text.equalsIgnoreCase("normal"))
			return new ImageCropperImpl();
		return null;
	}

	public GrayScaleConvertor getGrayScaleConveterInstance(String text) {
		if (text.equalsIgnoreCase("normal"))
			return new GrayScaleConvertorImpl();
		return null;
	}

	public Reader getReaderInstance(String text) {
		if (text.equalsIgnoreCase("zing"))
			return new BarcodeReader();
		return null;
	}

	public Rotater getRotatorInstance(String text) {
		if (text.equalsIgnoreCase("normal"))
			return new RotaterImpl();
		return null;
	}

	public TextReader getTextReaderInstance(String text) {
		if (text.equalsIgnoreCase("tessract"))
			return new TessractTextReader();
		return null;
	}

	public CompareImage getImageCompareInstance(String text) {
		if (text.equalsIgnoreCase("histogram"))
			return new HistogramCompareImage();
		else if (text.equalsIgnoreCase("pixel"))
			return new PixelImageComparison();
		return null;
	}
}
