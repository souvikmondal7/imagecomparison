package com.catalina.imagecomparison.services;

import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;

import static net.sourceforge.tess4j.ITessAPI.TessPageSegMode.PSM_SINGLE_BLOCK_VERT_TEXT;


public class TessractTextReader implements TextReader {

	@Override
	public String readText(Image image) {
		ITesseract tesseract = new Tesseract();
		tesseract.setDatapath("C:\\Users\\csauto4\\Documents\\EndToEndTesting\\E2Escript\\imagecomparison\\tessdata");
		try {
			String imgText = tesseract.doOCR(image.getImage());
			return imgText;
		} catch (TesseractException e) {
			e.getMessage();
			return "Error while reading image";
		}
	}

}
