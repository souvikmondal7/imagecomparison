package com.catalina.imagecomparison.services;


import com.google.zxing.*;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public class BarcodeReader implements Reader {

	@Override
	public String read(Image image) {
		// TODO Auto-generated method stub
		try {
			Map hintMap = new HashMap();
			hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
			hintMap.put(DecodeHintType.TRY_HARDER, Boolean.TRUE);
			hintMap.put(DecodeHintType.POSSIBLE_FORMATS, EnumSet.allOf(BarcodeFormat.class));
			//hintMap.put(DecodeHintType.PURE_BARCODE, Boolean.FALSE);

			BinaryBitmap binaryBitmap = new BinaryBitmap(
					new HybridBinarizer(new BufferedImageLuminanceSource(image.getImage())));
			Result qrCodeResult;

			qrCodeResult = new MultiFormatReader().decode(binaryBitmap, hintMap);
			return qrCodeResult.getText();
		} catch (NotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
}
