package com.catalina.imagecomparison.services;

public interface Reader {
	public String read(Image image);
}
