package com.catalina.imagecomparison.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.catalina.imagecomparison.entities.BarCode;
import com.catalina.imagecomparison.entities.ImageData;
import com.catalina.imagecomparison.entities.Message;
import com.catalina.imagecomparison.entities.QRCode;
import com.catalina.imagecomparison.entities.StaticImage;
import com.catalina.imagecomparison.services.impl.Binarization;
import com.catalina.imagecomparison.services.impl.ImageClearer;
import com.catalina.imagecomparison.services.impl.Invert;
import com.catalina.imagecomparison.services.impl.OCRPreProcessing;
import com.catalina.imagecomparison.services.impl.TextSharperImpl;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

public abstract class ImageCompartor {

	protected static int imageNO = 1;
	private static Logger logger = LoggerFactory.getLogger(ImageCompartor.class);
	private Factory factory = new Factory();

	public abstract ImageData compareImageFromJson(Image img, Image img2, ImageData data);

	protected void logSideBySideImage(Image img, Image temp, String Operation) {
		// String image = saveImage(temp, "Before"+Operation);
		String greyImage = saveImage(img, Operation + "Image" + imageNO++);
//		test.log(
//				LogStatus.INFO,
//				"<table><tr><th style=\"width:50%\">BEFORE</th><th style=\"width:50%\">After</th></tr><tr><td>"
//						+ test.addScreenCapture("./img/Before" + Operation
//								+(imageNO-1)+ ".jpg")
//						+ "</td><td>"
//						+ test.addScreenCapture(greyImage)
//						+ "<td></tr></table>");

	}

	protected String saveImage(Image img, String imageName) {
		img.writeImage(imageName, "jpg", "./report/img/");
		return "./img/" + imageName + ".jpg";
	}

	protected boolean Image(List<StaticImage> elements, Image img, Image img2) {
		// TODO Auto-generated method stub
		int i = imageNO;
		boolean result = true;
		for (StaticImage element : elements) {
			String startCord = element.getStartCord();
			String endCord = element.getEndCord();
			String startXY[] = startCord.split(",");
			String endXY[] = endCord.split(",");

			// Crop Image
			Cropper cropper = factory.getCropperInstance("normal");
			int x = Integer.parseInt(startXY[0]);
			int y = Integer.parseInt(startXY[1]);
			int height = Integer.parseInt(endXY[1]) - Integer.parseInt(startXY[1]);
			int width = Integer.parseInt(endXY[0]) - Integer.parseInt(startXY[0]);

			Image temp = cropper.cropImage(img, x, y, width, height);
			Image temp2 = cropper.cropImage(img2, x, y, width, height);

			logCompareSideBySideImage(temp2, temp, "Image" + ++i);
			CompareImage ci = factory.getImageCompareInstance("pixel");
			temp.writeImage("abc", "jpg", "./report");
			temp2.writeImage("abc2", "jpg", "./report");
			double diff = ci.compareImage(temp, temp2);
			if (diff < 98) {

				result &= false;
			}
		}
		return result;
	}

	protected void logCompareSideBySideImage(Image temp2, Image temp, String Operation) {
		// TODO Auto-generated method stub
		String image = saveImage(temp, "" + Operation + "image1");
		String image2 = saveImage(temp2, Operation + "image2");
//		test.log(
//				LogStatus.INFO,
//				"<table><tr><th style=\"width:50%\">First Image</th><th style=\"width:50%\">SecondImage</th></tr><tr><td>"
//						+ test.addScreenCapture(image)
//						+ "</td><td>"
//						+ test.addScreenCapture(image2) + "<td></tr></table>");

	}

	protected boolean comapreQRCodeValuesWithJson(Image img, QRCode qrCode) {
		// TODO Auto-generated method stub
		if (qrCode == null)
			return true;
		Reader reader = factory.getReaderInstance("zing");
		String qrCodeTextFomImage = reader.read(img);
		String qrCodeFromJson = qrCode.getMessage();
		logger.info("Got value from image :- " + qrCodeTextFomImage);
		if (qrCodeFromJson.equals(qrCodeTextFomImage)) {
			logger.info("qrCode values are not matched");
			return true;
		}
		return false;
	}

	protected List<Message> checkMessages(Image img, List<Message> list) {
		List<Message> resultList = new ArrayList<Message>();
		int i = 0;
		for (Message element : list) {
			logger.info("current message being checked:"+element.toString());
			boolean result = true;
			List<String> messages = element.getMessageGroup();
			StringBuilder message = new StringBuilder();
			for (String str : messages)
				message.append(str + "\n");
			System.out.println(element.toString());
			String startCord = element.getStartCord();
			String endCord = element.getEndCord();

			String startXY[] = startCord.split(",");
			String endXY[] = endCord.split(",");

			// Crop Image
			Cropper cropper = factory.getCropperInstance("normal");
			int x = Integer.parseInt(startXY[0]);
			int y = Integer.parseInt(startXY[1]);
			int height = Integer.parseInt(endXY[1]) - Integer.parseInt(startXY[1]);
			int width = Integer.parseInt(endXY[0]) - Integer.parseInt(startXY[0]);
			img.writeImage("temp", "png", "./");

			System.out.println(x + " " + y + " " + height + " " + width);
			Image temp = cropper.cropImage(img, x, y, width, height);

			if (element.getAlgorithm() != null)
				for (String algorithm : element.getAlgorithm()) {
					switch (algorithm) {
					case "zoom":
						Zoomer zoom = new Zoomer();
						temp = zoom.zoomImage(temp, 1.02F);
						break;
					case "textsharper":
						TextSharper sharper = new TextSharperImpl();
						temp = sharper.sharpeText(temp);
						break;
					case "binary":
						try {
							temp = Binarization.binarization(temp);
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						break;
					case "clear":
						try {
							temp = ImageClearer.temp(temp);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						break;
					case "invert":
						try {
							temp = Invert.inVert(temp);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						break;
					case "all":
						double length = 0;
						double width1 = 0;
						if (element.getResize() != null && element.getResize().length() >= 0) {
							String tempD[] = element.getResize().split(",");
							length = Double.parseDouble(tempD[0]);
							width1 = Double.parseDouble(tempD[1]);

						}
						try {
							temp = OCRPreProcessing.preProcessing(temp, length, width1);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						break;
					default:
						break;
					}
				}

			// temp = Zoomer.zoomImage(temp, (float) 1.02);

			// TextSharper ts = new TextSharperImpl();
			// temp = ts.sharpeText(temp);

			// read text
			// logSideBySideImage(test, temp, img, "SubImage" + i);
			TextReader reader = factory.getTextReaderInstance("tessract");
			String text = reader.readText(temp);

			logger.info("Test Extracted<br> " + text + "<br>Compared With<br>" + message.toString().replace("\n", ""));

			if (!message.toString().replace("\n", "").equals(text.replace("\n", ""))) {
				logger.info("Not able to match " + text.replace("\n", ""));
				result = false;
			}
			element.setExtractedMessage(text);
			element.setStatus(result);
			resultList.add(element);
		}

		return resultList;
	}

	protected boolean comapreBarCodeValuesWithJson(Image img, BarCode barCode) {
		// TODO Auto-generated method stub
		boolean result = true;

		String type = barCode.getType();
		String barCodeValue = barCode.getBarMessage();

		String imageBarCodeValue = "";
		Reader reader = factory.getReaderInstance("zing");
		switch (type) {
		case "UPC Code":
			imageBarCodeValue = reader.read(img);
			break;
		case "GSM1":
			break;
		}
		logger.info("Got value from image" + imageBarCodeValue);
		if (!barCodeValue.equals(imageBarCodeValue)) {
			logger.info("Value are not matched");
			result = false;
		}
		logger.info("Value are matched");
		return result;
	}

	public boolean compareImage(Image img, Image img2) {
		boolean result = true;

		// convert to gray scale
		GrayScaleConvertor gsc = factory.getGrayScaleConveterInstance("normal");
		img = gsc.convert(img);
		img2 = gsc.convert(img2);

		// compare bar code values
		if (!comapreBarCodeValues(img, img2))
			result &= false;

		// Get Current direction
		String direction = "";
		int rotate = 0;
		switch (direction.toLowerCase()) {
		case "left":
			rotate++;
		case "up":
			rotate++;
		case "right":
			rotate++;
			break;
		}

		Rotater rotater = factory.getRotatorInstance("normal");
		// rotate image as per requirement
		for (int i = 0; i < rotate; i++) {
			try {
				img = rotater.rotate(img);
				img2 = rotater.rotate(img2);
			} catch (IOException e) {
				System.out.println(e);
			}
		}

		// get horizonal elements
		// ArrayList<JsonElement> horizonal = getElementData("horizontal");

		// get vertical elements
		// ArrayList<JsonElement> vertical = getElementData("vertical");

		return result;

	}

	protected ArrayList<JsonElement> getElementData(String string, JsonArray array) {
		// TODO Auto-generated method stub
		ArrayList<JsonElement> list = new ArrayList<JsonElement>();
		if (array == null)
			return list;
		for (JsonElement element : array)
			list.add(element);
		return list;
	}

	public boolean comapreBarCodeValues(Image img, Image img2) {
		// read barcode values
		Reader reader = factory.getReaderInstance("zing");
		String firstBarCode = reader.read(img);
		String secondBarCode = reader.read(img2);
		if (!firstBarCode.equals(secondBarCode)) {
			System.out.println("Barcode Not Matched");
			return false;
		}
		return true;

	}
}
