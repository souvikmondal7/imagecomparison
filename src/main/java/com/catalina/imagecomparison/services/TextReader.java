package com.catalina.imagecomparison.services;

public interface TextReader {

	public String readText(Image image);
}
