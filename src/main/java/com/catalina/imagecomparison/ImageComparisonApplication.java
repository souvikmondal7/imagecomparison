package com.catalina.imagecomparison;

import org.opencv.core.Core;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ImageComparisonApplication implements ApplicationRunner {

	public static void main(String[] args) {
		SpringApplication.run(ImageComparisonApplication.class, args);
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		// System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		//System.load("D:\\MediaFiles\\imagecomparison\\imagecomparison\\opencv_java341.dll");
	}
}
