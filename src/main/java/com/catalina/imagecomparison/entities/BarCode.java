package com.catalina.imagecomparison.entities;

import lombok.*;

import javax.xml.bind.annotation.XmlRootElement;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@XmlRootElement
public class BarCode {
    String type;
    String barMessage;
    String startCord;
    String endCord;
    String extractedMessage;
    Boolean status;
}
