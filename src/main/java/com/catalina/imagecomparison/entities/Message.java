package com.catalina.imagecomparison.entities;

import lombok.*;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@XmlRootElement
public class Message {
	List<String> messageGroup;
	String startCord;
	String endCord;
	Boolean status;
	String extractedMessage;
	List<String> alternateMessages;

	@Override
	public String toString() {
		return "Message{" +
				"messageGroup=" + messageGroup +
				", startCord='" + startCord + '\'' +
				", endCord='" + endCord + '\'' +
				", status=" + status +
				", extractedMessage='" + extractedMessage + '\'' +
				", alternateMessages=" + alternateMessages +
				", algorithm=" + algorithm +
				", resize='" + resize + '\'' +
				'}';
	}

	List<String> algorithm;
	String resize;
}
