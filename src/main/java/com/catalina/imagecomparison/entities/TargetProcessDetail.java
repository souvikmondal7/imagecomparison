package com.catalina.imagecomparison.entities;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TargetProcessDetail {
	private String testPlanId;
	private String testPlanBuildId;
	private String testCaseId;
	private String status;
	private String comment;
	private String testCaseRunId;

	public String getTestPlanId() {
		return testPlanId;
	}

	public void setTestPlanId(String testPlanId) {
		this.testPlanId = testPlanId;
	}

	public String getTestPlanBuildId() {
		return testPlanBuildId;
	}

	public void setTestPlanBuildId(String testPlanBuildId) {
		this.testPlanBuildId = testPlanBuildId;
	}

	public String getTestCaseId() {
		return testCaseId;
	}

	public void setTestCaseId(String testCaseId) {
		this.testCaseId = testCaseId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getTestCaseRunId() {
		return testCaseRunId;
	}

	public void setTestCaseRunId(String testCaseRunId) {
		this.testCaseRunId = testCaseRunId;
	}

	public TargetProcessDetail(String testPlanId, String testPlanBuildId, String testCaseId, String status,
			String comment, String testCaseRunId) {
		super();
		this.testPlanId = testPlanId;
		this.testPlanBuildId = testPlanBuildId;
		this.testCaseId = testCaseId;
		this.status = status;
		this.comment = comment;
		this.testCaseRunId = testCaseRunId;
	}

	public TargetProcessDetail() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "TargetProcessDetail [testPlanId=" + testPlanId + ", testPlanBuildId=" + testPlanBuildId
				+ ", testCaseId=" + testCaseId + ", status=" + status + ", comment=" + comment + ", testCaseRunId="
				+ testCaseRunId + "]";
	}

}
