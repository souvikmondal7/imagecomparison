package com.catalina.imagecomparison.entities;

import lombok.*;

import javax.xml.bind.annotation.XmlRootElement;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@XmlRootElement
public class QRCode {
    String type;
    String message;
    String startCord;
    String endCord;
    Boolean status;
}
