package com.catalina.imagecomparison.entities;

import lombok.*;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class ImageData {
	String imagePathSource;
	String imagePathTarget;
	String imageOrientation;
	String type;
	Integer verticalRotate;
	List<Message> horizontalMessages;

	@Override
	public String toString() {
		return "ImageData{" +
				"imagePathSource='" + imagePathSource + '\'' +
				", imagePathTarget='" + imagePathTarget + '\'' +
				", imageOrientation='" + imageOrientation + '\'' +
				", type='" + type + '\'' +
				", verticalRotate=" + verticalRotate +
				", horizontalMessages=" + horizontalMessages +
				", verticalMessages=" + verticalMessages +
				", barCode=" + barCode +
				", qrCode=" + qrCode +
				", expiryDate='" + expiryDate + '\'' +
				", staticImageSections=" + staticImageSections +
				", status=" + status +
				", targetProcessDetail=" + targetProcessDetail +
				'}';
	}

	List<Message> verticalMessages;
	BarCode barCode;
	QRCode qrCode;
	String expiryDate;
	List<StaticImage> staticImageSections;
	Boolean status;
	TargetProcessDetail targetProcessDetail;
}
