package com.catalina.imagecomparison.resources;

import com.catalina.imagecomparison.entities.ImageData;
import com.catalina.imagecomparison.services.Image;
import com.catalina.imagecomparison.services.ImageCompartor;
import com.catalina.imagecomparison.services.impl.ImageComparatorImpl;
import com.catalina.imagecomparison.utils.FileUtil;
import org.opencv.core.Core;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
public class ImageCompResource {

    private static Logger logger = LoggerFactory.getLogger(ImageCompResource.class);


    @PostMapping(value = "/compare",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ImageData> compare(@RequestBody ImageData data){
        //System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        try {
            logger.info("Running process for file "+ data);
            ImageData result = traverseJson(data);
            // TODO change the response according to data
            return new ResponseEntity<ImageData>(result, HttpStatus.OK);
        } catch (IOException e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    public static ImageData traverseJson(ImageData data)
            throws IOException {

        // TODO: Log this entry - the name of teh JSON picked for comparison
        // read json file
        // String jsonFile =
        // "\\\\test_storage1\\spliceshare\\splice\\Coupon\\imageJson2.json";
//        test.log(LogStatus.INFO,
//                "Loading the file to execute code<br>" + file.getAbsolutePath());
//
//        // File file = new File(jsonFile);
//        String jsonContent = ConvertFileToString.convert(file);
        // parsing json file to objects

        // TODO: Log this entry
        // Lets add one more entry into teh JSON that tells us the expected
        // image name
        // in the /tmp/lane_* path.
        // This should indicate which image file should we pick up if multiple
        // image
        // files are generated
        // The image name can be a pattern as well. This will help us pick the
        // file if
        // the image name has a dynamic aspect to it
        // this file is referenced file
        String imagePath = data.getImagePathSource();
        String typeOfFile = data.getType();
        // extract componenets
        String path = imagePath.substring(0,
                imagePath.lastIndexOf("\\"));
        String imageName = imagePath.substring(
                imagePath.lastIndexOf("\\"),
                imagePath.lastIndexOf("."));
        String extension = imagePath.substring(
                imagePath.lastIndexOf(".") + 1);

        // TODO: Please change the code to pick the dynamic image created in the
        // /tmp/lane_*
        // read images
        // first image

        Image img = new Image();
        img.readImage(imageName, extension, path);

        imagePath = data.getImagePathTarget();

        // extract componenets
        path = imagePath.substring(0,
                imagePath.lastIndexOf("\\"));
        imageName = "";
        extension = imagePath.substring(
                imagePath.lastIndexOf(".") + 1);

        // this image is gennerated by coup or webpos
        // getting file name depending on the type
        // in case of pattern it will pick last file in the pattern
        imageName = findFileByType(imagePath, typeOfFile, path);

        // second image
        System.out.println(path + " " + imageName + " " + extension);
        logger.info("imagePath for new image:"+path+"--name of new image:"+imageName+"--extension for new image:"+extension);
        Image img2 = new Image();
        img2.readImage(imageName, extension, path);



        // TODO: For logging part create separate ExtentTest for each type of
        // comparision. We can expand it as and when we grow further
        // comapare image
        // if (!compareImage(img, img2))
        // return;
        ImageCompartor imageCompartor = new ImageComparatorImpl();
        ImageData result= imageCompartor.compareImageFromJson(img, img2, data);

        if (!result.getStatus()) {
            // return false;
            logger.info("Test fail");
        } else {
            logger.info("Test pass");
        }

        /*
         * GrayScaleConvertor gsc = new GrayScaleConvertor(); img =
         * gsc.convertColorToGray(img); img2 = gsc.convertColorToGray(img2);
         *
         * CompareImage compare = new CompareImage(); double diff =
         * compare.compareImage(img, img2);
         *
         * test.log(LogStatus.INFO,"Similarity in images is " + diff +"%" );
         */
        // return true;
        return result;
    }

    private static String findFileByType(String imagePath,
                                         String typeOfFile, String path) {
        String name = null;
        final String substring = imagePath.substring(
                imagePath.lastIndexOf("\\"),
                imagePath.lastIndexOf("."));
        switch (typeOfFile.toLowerCase()) {
            case "pattern":
                name = FileUtil.getFileByPattern(
                        path,
                        substring);
                break;
            case "absolute":
                name = substring;
                break;
        }
        return name;
    }
}
